
alter database test 
	set single_user with
		rollback immediate

use master
	restore database 
		from disk='c\backup'
			with replace
			